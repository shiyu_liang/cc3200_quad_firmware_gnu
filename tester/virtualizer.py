from __future__ import division

import sys
import os
import subprocess
import numpy as np
import ctypes
import wx
import serial
import string
import time
import pylab
from visual import *
from visual.graph import *
from math import *
import matplotlib
matplotlib.use("wx")
from matplotlib import pyplot as plt
from matplotlib import animation
from serial import SerialException

read_UART = 1

virt_H = 360
virt_W = 480
graph_H = 360
graph_W = 480
pad = 20
window_pad = 10
pitchRad = 0
rollRad = 0
yawRad = 0
graph_points = graph_W / 2

pitchLabel = "Pitch: "
rollLabel = "Roll: "
pitchData = [0] * int(graph_points)
rollData = [0] * int(graph_points)

comPort = 'COM11'
baudRate = 115200

# TODO: review this
x = np.linspace(0, 6*np.pi, 100)
y = np.linspace(0, 0, 100)
#plt.ion()
fig = plt.figure()
plt.pause(0.001)
thismanager = plt.get_current_fig_manager()
plt.pause(0.001)
thismanager.window.wm_geometry("960x360+0+380")
plt.pause(0.001)

p_ax = fig.add_subplot(121)
p_ax.set_title('pitch degrees')
p_a, = p_ax.plot(x, y, 'r-')
p_g, = p_ax.plot(x, y, 'b-')
p_k, = p_ax.plot(x, y, 'g-')
plt.legend([p_a, p_g, p_k], ['accelerometer', 'gyro', 'kalman filter'])
plt.pause(0.001)
pylab.ylim([-180,180])

r_ax = fig.add_subplot(122)
r_ax.set_title('roll degrees')
r_a, = r_ax.plot(x, y, 'r-')
r_g, = r_ax.plot(x, y, 'b-')
r_k, = r_ax.plot(x, y, 'g-')
plt.legend([r_a, r_g, r_k], ['accelerometer', 'gyro', 'kalman filter'])
plt.pause(0.001)
pylab.ylim([-180,180])

dlen = len(p_a.get_ydata())

def safeClose(evt):
	global fig
	print "safely closing"
	#os.killpg(roll_proc.pid, signal.SIGTERM)
	#roll_proc.kill()
	#pitch_proc.kill()
	plt.close(fig)
	exit()
	sys.exit(0)

if __name__ == "__main__":
	print "Running " + str(ctypes.sizeof(ctypes.c_voidp)*8) + " bit Python"


	m_window = window( width = 2 * (virt_W + window.dwidth),
			height = virt_H + window.dheight + window.menuheight,
			menus=True,
			title="CC3200 Quadcopter Virtualizer",
			style=wx.SYSTEM_MENU | wx.CAPTION
		)

	v_disp = display( window = m_window,
			x = pad,
			y = pad,
			width = virt_W - 2 * pad,
			height = virt_H - 2 * pad,
			forward = vector(0,0,-1)
		)

	#anim = animation.FuncAnimation(rollFig, plot_animate, init_func=plot_init,
	#	frames=None, interval=20, blit=True, repeat=True)

	v_disp.userzoom = False
	v_disp.userspin = False
	v_disp.autoscale = False
	v_disp.range = (15,15,15)
	v_disp.background=(0.5,0.5,0.5)


	# set up panel to display intput data
	p_data = m_window.panel
	error_text = wx.StaticText(p_data, pos=(0, 0), label="")

	closeButton = wx.Button(p_data, label='Close Safely', pos=(virt_W,virt_H - 2*pad))
	closeButton.Bind(wx.EVT_BUTTON, safeClose)

	wx.StaticText(p_data, pos=(virt_W,pad-15), size=(pad*3,pad), label='motor 4',
		style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
	s4 = wx.Slider(p_data, pos=(virt_W,pad), size=(200,20), minValue=0, maxValue=255)

	wx.StaticText(p_data, pos=(virt_W + 220,pad-15), size=(pad*3,pad), label='motor 3',
		style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
	s3 = wx.Slider(p_data, pos=(virt_W + 220,pad), size=(200,20), minValue=0, maxValue=255)

	wx.StaticText(p_data, pos=(virt_W,pad+100-15), size=(pad*3,pad), label='motor 1',
		style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
	s1 = wx.Slider(p_data, pos=(virt_W,pad + 100), size=(200,20), minValue=0, maxValue=255)

	wx.StaticText(p_data, pos=(virt_W + 220,pad+100-15), size=(pad*3,pad), label='motor 2',
		style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
	s2 = wx.Slider(p_data, pos=(virt_W + 220,pad + 100), size=(200,20), minValue=0, maxValue=255)

	# set up virtualization area
	quad_f = frame(axis=(1,0,0))

	quad = box(frame = quad_f, pos=(0,0,0), size=(4.4, 0.2, 5.5), color=color.magenta)
	cc3200 = box(frame = quad_f, pos=(0,0.2,0), size=(1, 0.1, 1), color=color.black)
	wings1 = box(frame = quad_f, pos=(0,0,0), axis=(1,0,1), color=color.magenta, height=0.2, length=10, width=0.4)
	wings2 = box(frame = quad_f, pos=(0,0,0), axis=(-1,0,1), color=color.magenta, height=0.2, length=10, width=0.4)
	cap_small = cylinder(frame = quad_f, pos=(-1.5,0,2.75),axis=(0,0,1), radius=0.25, color=(0.2,0.2,0.2))
	cap_large = cylinder(frame = quad_f, pos=(1.5,0,2.75),axis=(0,0,1.5), radius=0.35, color=color.blue)
	straight_up = [(0,0,0), (0,0.5,0)]
	rotor_1 = shapes.circle(pos=(-4.5,4.5), radius=2)
	rotor_2 = shapes.circle(pos=(4.5,4.5), radius=2)
	rotor_3 = shapes.circle(pos=(-4.5,-4.5), radius=2)
	rotor_4 = shapes.circle(pos=(4.5,-4.5), radius=2)
	extrusion(frame = quad_f, pos = straight_up, shape = rotor_1, color=color.red)
	extrusion(frame = quad_f, pos = straight_up, shape = rotor_2, color=color.red)
	extrusion(frame = quad_f, pos = straight_up, shape = rotor_3, color=color.red)
	extrusion(frame = quad_f, pos = straight_up, shape = rotor_4, color=color.red)

	arrow_x = arrow(pos = (-15, -10, -5), axis = (4,0,0), shaftwidth=0.5, color=color.red)
	arrow_y = arrow(pos = (-15, -10, -5), axis = (0,4,0), shaftwidth=0.5, color=color.green)
	arrow_z = arrow(pos = (-15, -10, -5), axis = (0,0,4), shaftwidth=0.5, color=color.blue)

	# set up serial port
	if (read_UART):
		try:
			serialPort = serial.Serial(port = comPort, baudrate = baudRate, timeout = 1)
			serialPort.flushInput()    # clean input buffer
			line = serialPort.readline()
		except Exception as e:
			print e
			exit

	# main loop
	while 1:
		rate(30)

		if (read_UART):
			try:
				serialPort.write("w")
				line = serialPort.readline().rstrip('\n').rstrip('\r')
				#print line
				input_array = string.split(line,",")    # Fields split
				gyro_pitch = float(input_array[0])
				gyro_roll = float(input_array[1])
				accl_pitch = float(input_array[2])
				accl_roll = float(input_array[3])
				k_pitch = float(input_array[4])
				k_roll = float(input_array[5])
				m1 = int(input_array[6])
				m2 = int(input_array[7])
				m3 = int(input_array[8])
				m4 = int(input_array[9])

				yd = p_a.get_ydata()
				y_0 = int(accl_pitch)
				tmp_y = yd[1:dlen]
				new_y = np.append(tmp_y, y_0)
				p_a.set_ydata(new_y)

				yd = p_g.get_ydata()
				y_0 = int(gyro_pitch)
				tmp_y = yd[1:dlen]
				new_y = np.append(tmp_y, y_0)
				p_g.set_ydata(new_y)

				yd = r_a.get_ydata()
				y_0 = int(accl_roll)
				tmp_y = yd[1:dlen]
				new_y = np.append(tmp_y, y_0)
				r_a.set_ydata(new_y)

				yd = r_g.get_ydata()
				y_0 = int(gyro_roll)
				tmp_y = yd[1:dlen]
				new_y = np.append(tmp_y, y_0)
				r_g.set_ydata(new_y)

				yd = p_k.get_ydata()
				y_0 = int(k_pitch)
				tmp_y = yd[1:dlen]
				new_y = np.append(tmp_y, y_0)
				p_k.set_ydata(new_y)

				yd = r_k.get_ydata()
				y_0 = int(k_roll)
				tmp_y = yd[1:dlen]
				new_y = np.append(tmp_y, y_0)
				r_k.set_ydata(new_y)

				fig.canvas.draw()

				rollRad = -radians(k_roll)
				pitchRad = radians(k_pitch)

				s1.SetValue(m1)
				s2.SetValue(m2)
				s3.SetValue(m3)
				s4.SetValue(m4)

			except SerialException as e:
				print "Error: " + e.message
				error_text.SetLabel("Error: " + e.message)
				error_text.SetForegroundColour((255,0,0))
			except ValueError as e:
				continue
				print line
				print "Error: " + e.message
				print "Error: " + e.message
				error_text.SetLabel("Error: " + e.message)
				error_text.SetForegroundColour((255,0,0))
			except Exception:
				continue

			# adjust the model accordingly
			quad_f.up=(cos(pitchRad)*sin(rollRad),cos(pitchRad)*cos(rollRad),-sin(pitchRad))
			quad_f.axis=(sin(yawRad)*sin(pitchRad)*sin(rollRad) + cos(yawRad)*cos(rollRad),
				sin(yawRad)*sin(pitchRad)*cos(rollRad) - cos(yawRad)*sin(rollRad),
				sin(yawRad)*cos(pitchRad))
