#include <stdio.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "rom_map.h"
#include "gpio.h"
#include "led.h"

#define OFF 0
#define ON 1

int ledBlueStatus;
int ledGreenStatus;
int ledOrangeStatus;

void initLEDs() {
    clearBlue();
    clearGreen();
    clearOrange();
}

void setBlue() {
    ledBlueStatus = ON;
    MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_0, GPIO_PIN_0);
}

void setGreen() {
    ledGreenStatus = ON;
    MAP_GPIOPinWrite(GPIOA0_BASE, GPIO_PIN_7, GPIO_PIN_7);
}

void setOrange() {
    ledOrangeStatus = ON;
    MAP_GPIOPinWrite(GPIOA0_BASE, GPIO_PIN_6, GPIO_PIN_6);
}

void clearBlue() {
    ledBlueStatus = OFF;
    MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_0, 0);
}

void clearGreen() {
    ledGreenStatus = OFF;
    MAP_GPIOPinWrite(GPIOA0_BASE, GPIO_PIN_7, 0);
}

void clearOrange() {
    ledOrangeStatus = OFF;
    MAP_GPIOPinWrite(GPIOA0_BASE, GPIO_PIN_6, 0);
}

void toggleBlue() {
    if (ledBlueStatus)
        clearBlue();
    else
        setBlue();
}

void toggleGreen() {
    if (ledGreenStatus)
        clearGreen();
    else
        setGreen();
}

void toggleOrange() {
    if (ledOrangeStatus)
        clearOrange();
    else
        setOrange();
}
