#ifndef __LED_H__
#define __LED_H__

void initLEDs();

void setBlue();
void setGreen();
void setOrange();

void clearBlue();
void clearGreen();
void clearOrange();

void toggleBlue();
void toggleGreen();
void toggleOrange();

#endif
