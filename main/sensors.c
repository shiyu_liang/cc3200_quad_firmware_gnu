/*
    - file name gyro.c is very misleading, this is because the only i2c devices
    working on main2.0 was the gyroscope...
    - But this file actually does general i2c functions including an
    accelerometer
*/

// Standard includes
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "hw_i2c.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"
#include "prcm.h"
#include "utils.h"
#include "uart.h"
#include "i2c.h"

// Common interface includes
#include "uart_if.h"
#include "i2c_if.h"

// FreeRTOS includes
#include "osi.h"
#include "FreeRTOS.h"
#include "task.h"

// local includes
#include "pinmux.h"
#include "sensors.h"
#include "led.h"

/*/////////////////////////////////////////////////////////////////////////////
    Defines
/////////////////////////////////////////////////////////////////////////////*/

#define I2C_BASE                I2CA0_BASE
#define UART_PRINT              Report
#define GYRO_I2C_ADDR           0x20
#define ACCL_I2C_ADDR           0x1D
#define I2C_BUF_SIZE            256
#define WHOAMI_VAL              0xd7
#define PI                      3.14159265

/*/////////////////////////////////////////////////////////////////////////////
    Variables
/////////////////////////////////////////////////////////////////////////////*/

unsigned char i2cBuf[I2C_BUF_SIZE];
unsigned char i2cDataBuf[I2C_BUF_SIZE];
char gyro_found;
char accl_found;
extern unsigned g_gyro_cycle_count;

/*/////////////////////////////////////////////////////////////////////////////
    Function Prototypes
/////////////////////////////////////////////////////////////////////////////*/

void sensors_init();
void sensors_reset();
void sensors_configure();

/*/////////////////////////////////////////////////////////////////////////////
    Function Definitions
/////////////////////////////////////////////////////////////////////////////*/

void sensors_reset() {
    int i;

    if (gyro_found) {
        UART_PRINT("Resetting Gyro\r\n");
        i = 0;
        i2cBuf[i++] = 0x13;   // register address
        i2cBuf[i++] = 040;    // FIFO in circular buffer mode
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, i, 1);
    }

    if (accl_found) {
        UART_PRINT("Resetting Accelerometer\r\n");
        i = 0;
        i2cBuf[i++] = 0x2B;   // register address
        i2cBuf[i++] = 0x40;   // reset the device
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, i, 1);
    }
}

void sensors_configure() {
    int i;

    if (gyro_found) {
        i = 0;
        UART_PRINT("Configuring Gyro\r\n");
        i2cBuf[i++] = 0x09;   // register address
        i2cBuf[i++] = 0x40;   // use circular buffer
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, i, 1);

        i = 0;
        i2cBuf[i++] = 0x13;   // register address
        i2cBuf[i++] = 0x01;   // to ready mode, ODR = 800Hz
        i2cBuf[i++] = 0x00;   // no interrupt/pin set
        i2cBuf[i++] = 0x08;   // wrap to 0x01 when reading data registers
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, i, 1);

        i = 0;
        i2cBuf[i++] = 0x0D;   // register address
        i2cBuf[i++] = 0x00;   // FSR = +- 2000 dps
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, i, 1);
    }

    if (accl_found) {
        UART_PRINT("Configuring Accelerometer\r\n");

        i = 0;
        i2cBuf[i++] = 0x2A;   // register address
        i2cBuf[i++] = 0x01;   //
        I2C_IF_Write(ACCL_I2C_ADDR, &i2cBuf, i, 1);
    }
}

void sensors_init() {

    UART_PRINT("Initializing Gyro\r\n");
    unsigned char reg_addr = 0x0c;
    gyro_found = accl_found = 0;

    I2C_IF_Open(I2C_MASTER_MODE_FST);

    I2C_IF_ReadFrom(GYRO_I2C_ADDR, &reg_addr, 1, &i2cBuf, 1);
    if (i2cBuf[0] == WHOAMI_VAL) {
        UART_PRINT("! FXAS21002 Gyro found\r\n");
        gyro_found = 1;
    }

    UART_PRINT("Initializing Accelerometer\r\n");
    reg_addr = 0x0D;
    I2C_IF_ReadFrom(ACCL_I2C_ADDR, &reg_addr, 1, &i2cBuf, 1);
    if (i2cBuf[0] == 0x2A) {
        UART_PRINT("! MMA8452Q Accelerometer found\r\n");
        accl_found = 1;
    }

    sensors_reset();

    sensors_configure();
}

// reads and updates sensor readings
void sensors_update() {
    // read out 10 data samples from the gyro and average out
    // update current average value. Set this up as a task that runs
    // constantly - so to get constant gyro readings
    unsigned char i, reg_addr;
    short tmp;

    if (gyro_found) {
        // set to active mode
        i2cBuf[0] = 0x13;   // register address
        i2cBuf[1] = 0x02;   // set to active mode
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, 2, 1);

        // read data values
        reg_addr = 0x01;
        for (i=0; i<6; i++) {
            I2C_IF_ReadFrom(GYRO_I2C_ADDR, &reg_addr, 1, &i2cDataBuf[i], 1);
            reg_addr++;
        }

        tmp = i2cDataBuf[0] << 8 | i2cDataBuf[1];
        gyro_x = (gyro_x + tmp) / 2;
        tmp = i2cDataBuf[2] << 8 | i2cDataBuf[3];
        gyro_y = (gyro_y + tmp) / 2;
        tmp = i2cDataBuf[4] << 8 | i2cDataBuf[5];
        gyro_z = (gyro_z + tmp) / 2;

        // flip X and Y readings since sensor is facing backward
        gyro_x *= -1;
        gyro_y *= -1;

        i2cBuf[0] = 0x13;   // register address
        i2cBuf[1] = 0x02;   // set to ready mode
        I2C_IF_Write(GYRO_I2C_ADDR, &i2cBuf, 2, 1);
    }

    if (accl_found) {
        reg_addr = 0x00;
        I2C_IF_ReadFrom(ACCL_I2C_ADDR, &reg_addr, 1, &i2cDataBuf, 1);

        reg_addr = 0x01;
        for (i=0; i<6; i++) {
            I2C_IF_ReadFrom(ACCL_I2C_ADDR, &reg_addr, 1, &i2cDataBuf[i], 1);
            reg_addr++;
        }

        accl_x = (i2cDataBuf[0] << 4) | (i2cDataBuf[1] >> 4);
        accl_x = (accl_x & 0x800)? accl_x | 0xfffff000 : accl_x;

        accl_y = (i2cDataBuf[2] << 4) | (i2cDataBuf[3] >> 4);
        accl_y = (accl_y & 0x800)? accl_y | 0xfffff000 : accl_y;

        accl_z = (i2cDataBuf[4] << 4) | (i2cDataBuf[5] >> 4);
        accl_z = (accl_z & 0x800)? accl_z | 0xfffff000 : accl_z;
    }

    if (gyro_found && accl_found)
        setBlue();
    else
        clearBlue();
}
