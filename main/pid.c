#include "pid.h"

void init_pid(struct pwm_pid * pid, float kp, float ki, float kd){
    pid->Kp = kp;
    pid->Ki = ki;
    pid->Kd = kd;
    pid->cur = pid->prev = pid->sum = 0;
}

float pid_update(struct pwm_pid * pid, float err, float dt) {
    float ret, diff;
    diff = err - pid->prev;
    // LPF on integral error
    pid->sum += diff;
    ret = pid->Kp * err;
    ret += pid->Ki * pid->sum;
    // LPF on derivative error
    ret += pid->Kd * diff/dt;
    pid->prev = err;
    return ret;
}
