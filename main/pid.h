#ifndef __PID_H__
#define __PID_H__

struct pwm_pid {
    float Kp, Ki, Kd;
    float cur, prev;
    float sum;
};

void init_pid(struct pwm_pid*, float kp, float ki, float kd);
float pid_update(struct pwm_pid*, float err, float dt);

#endif
