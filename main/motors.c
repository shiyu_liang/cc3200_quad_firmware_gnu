#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_apps_rcm.h"
#include "hw_common_reg.h"
#include "interrupt.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
#include "timer.h"
#include "utils.h"
#include "prcm.h"
#include "uart.h"

#include "uart_if.h"
#include "timer_if.h"

// FreeRTOS includes
#include "osi.h"
#include "FreeRTOS.h"
#include "task.h"

#include "pinmux.h"
#include "sensors.h"
#include "common.h"
#include "packet.h"
#include "ars.h"
#include "pid.h"
#include "wifi.h"

/*/////////////////////////////////////////////////////////////////////////////
    Defines
/////////////////////////////////////////////////////////////////////////////*/

#define TIMER_INTERVAL_RELOAD   19890 // = (255*78)
#define DUTYCYCLE_GRANULARITY   78
#define PWM_UPDATE_STEP         5
#define VERTICAL_ADJT           10
#define P_R_ADJT                30
#define HIGH_PRIORITY           0x00
#define LOW_PRIORITY            0xFF
#define GPT_PRESCALE_VAL        0
#define GPT_CLK_PERIOD_MD       0.8192
#define PI                      3.14159265
#define BIT_DPS                 0.0625
#define GYROY_CALIBRATION       2.8
#define GYROX_CALIBRATION       2.5
#define PID_KP                  1.0
#define PID_KI                  0.0
#define PID_KD                  0.0

/*/////////////////////////////////////////////////////////////////////////////
    Variables
/////////////////////////////////////////////////////////////////////////////*/

extern int gyro_x;
extern int gyro_y;
extern int gyro_z;

extern int accl_x;
extern int accl_y;
extern int accl_z;

float gyro_roll, accl_roll, k_roll;
float gyro_pitch, accl_pitch, k_pitch;

extern struct UdpCmd udpCmdRecvStruct;

int timer_int_cnt;

struct motor_pwm_targets {
    int motor_1;
    int motor_2;
    int motor_3;
    int motor_4;
} mTagetPWMs;

struct Gyro1DKalman filter_roll;
struct Gyro1DKalman filter_pitch;

struct pwm_pid pid_roll;
struct pwm_pid pid_pitch;

extern unsigned char g_wifiInitialized;

/*/////////////////////////////////////////////////////////////////////////////
    Function Prototypes
/////////////////////////////////////////////////////////////////////////////*/

void timer_int_handler();
void timer_init();
unsigned long timer_get_val();

/*/////////////////////////////////////////////////////////////////////////////
    Function Definitions
/////////////////////////////////////////////////////////////////////////////*/

void trimPWM(int * ptr) {
    if (*ptr > 0xff)
        *ptr = 0xff;
    if (*ptr <= 0)
        *ptr = 0;
}

void UpdateDutyCycle(unsigned long ulBase, unsigned long ulTimer,
                     unsigned char ucLevel)
{
    //
    // Match value is updated to reflect the new dutycycle settings
    //
    MAP_TimerMatchSet(ulBase,ulTimer,(ucLevel*DUTYCYCLE_GRANULARITY));
}

void SetupTimerPWMMode(unsigned long ulBase, unsigned long ulTimer,
                       unsigned long ulConfig, unsigned char ucInvert)
{
    //
    // Set GPT - Configured Timer in PWM mode.
    //
    MAP_TimerConfigure(ulBase,ulConfig);
    MAP_TimerPrescaleSet(ulBase,ulTimer,0);

    //
    // Inverting the timer output if required
    //
    MAP_TimerControlLevel(ulBase,ulTimer,ucInvert);

    //
    // Load value set to ~0.5 ms time period
    //
    MAP_TimerLoadSet(ulBase,ulTimer,TIMER_INTERVAL_RELOAD);

    //
    // Match value set so as to output level 0
    //
    MAP_TimerMatchSet(ulBase,ulTimer,TIMER_INTERVAL_RELOAD);

}

void motors_init() {
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA0, PRCM_RUN_MODE_CLK); // PWM
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK); // PWM
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK); // PWM

    SetupTimerPWMMode(TIMERA0_BASE, TIMER_A,
            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM), 1);

    SetupTimerPWMMode(TIMERA2_BASE, TIMER_B,
            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM), 1);

    SetupTimerPWMMode(TIMERA3_BASE, TIMER_A,
            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1);

    SetupTimerPWMMode(TIMERA3_BASE, TIMER_B,
            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1);

    MAP_TimerEnable(TIMERA0_BASE,TIMER_A);
    MAP_TimerEnable(TIMERA2_BASE,TIMER_B);
    MAP_TimerEnable(TIMERA3_BASE,TIMER_A);
    MAP_TimerEnable(TIMERA3_BASE,TIMER_B);

    gyro_roll = accl_roll = k_roll = 0;
    gyro_pitch = accl_pitch = k_pitch = 0;

    UART_PRINT("Initializing Timer\r\n");
    timer_init();
}


void timer_int_handler() {
    Timer_IF_Stop(TIMERA1_BASE, TIMER_BOTH);
    Timer_IF_DeInit(TIMERA1_BASE, TIMER_BOTH);
    timer_int_cnt++;
    timer_init();
}

void timer_init() {
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA1, PRCM_RUN_MODE_CLK);
    Timer_IF_Init(PRCM_TIMERA1, TIMERA1_BASE, TIMER_CFG_ONE_SHOT_UP, TIMER_BOTH, GPT_PRESCALE_VAL);
    Timer_IF_IntSetup(TIMERA1_BASE, TIMER_BOTH, timer_int_handler);

    Timer_IF_Start(TIMERA1_BASE, TIMER_BOTH, 0xffff);
    timer_int_cnt = 0;
}

unsigned long timer_get_val() {
    unsigned long ret = MAP_TimerValueGet(TIMERA1_BASE, TIMER_BOTH);
    Timer_IF_Stop(TIMERA1_BASE, TIMER_BOTH);
    Timer_IF_DeInit(TIMERA1_BASE, TIMER_BOTH);
    timer_init();
    if (timer_int_cnt)
        UART_PRINT("timer oveflow occured\r\n");
    return ret;
}

void motors_ctrl(void *pvParameters) {
    float dt;   // change in time in seconds
    float pitch_diff, roll_diff;
    float pitch_diff_old, roll_diff_old;

    init_Gyro1DKalman(&filter_pitch, 0.0001, 0.0003, 10);
    init_Gyro1DKalman(&filter_roll, 0.0001, 0.0003, 10);

    init_pid(&pid_roll, PID_KP, PID_KI, PID_KD);
    init_pid(&pid_pitch, PID_KP, PID_KI, PID_KD);

    while(1)
    {
        // update gyro and accelerometer readings
        sensors_update();

        // get time passed since last call
        dt = (float) timer_get_val() * GPT_CLK_PERIOD_MD /1000;

        // Pitch & Roll from integrating Gyro signals
        gyro_pitch += (float)gyro_x * dt * BIT_DPS * GYROX_CALIBRATION;
        gyro_roll += (float)gyro_y * dt * BIT_DPS * GYROY_CALIBRATION;

        // Pitch & Roll from Accelerometer Measurements
        accl_pitch = -atan((double)accl_y/accl_z) * 180/PI;
        accl_roll = atan((double)accl_x/accl_z) * 180/PI;

        // Kalman filter predictions
        ars_predict(&filter_pitch, (float)gyro_x * BIT_DPS * GYROX_CALIBRATION * PI/180, dt);
        k_pitch = ars_update(&filter_pitch, accl_pitch);
        ars_predict(&filter_roll, (float)gyro_y * BIT_DPS * GYROY_CALIBRATION * PI/180, dt);
        k_roll = ars_update(&filter_roll, accl_roll);

        // TODO - get target pitch & roll from controller
        pitch_diff = 0 - k_pitch;
        roll_diff = 0 - k_roll;

        pitch_diff = (pid_update(&pid_roll, pitch_diff, dt) + pitch_diff_old)/2;
        pitch_diff_old = pitch_diff;
        roll_diff = (pid_update(&pid_pitch, roll_diff, dt) + roll_diff_old)/2;
        roll_diff_old = roll_diff;

        // Motor PIDs
        mTagetPWMs.motor_1 = udpCmdRecvStruct._y_Left
            + (int) pitch_diff
            - (int) roll_diff;
        trimPWM(&mTagetPWMs.motor_1);

        mTagetPWMs.motor_2 = udpCmdRecvStruct._y_Left
            + (int) pitch_diff
            + (int) roll_diff;
        trimPWM(&mTagetPWMs.motor_2);

        mTagetPWMs.motor_3 = udpCmdRecvStruct._y_Left
            - (int) pitch_diff
            + (int) roll_diff;
        trimPWM(&mTagetPWMs.motor_3);

        mTagetPWMs.motor_4 = udpCmdRecvStruct._y_Left
            - (int) pitch_diff
            - (int) roll_diff;
        trimPWM(&mTagetPWMs.motor_4);

        // Log interface with virtualizer, Python script under ../test/
        if(MAP_UARTCharGetNonBlocking(CONSOLE) == 'w')
            UART_PRINT("%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%u,%u,%u,%u\r\n",
                gyro_pitch, gyro_roll, accl_pitch,
                accl_roll, k_pitch, k_roll,
                mTagetPWMs.motor_1, mTagetPWMs.motor_2,
                mTagetPWMs.motor_3, mTagetPWMs.motor_4);

        // update current PWMs gradually
        UpdateDutyCycle(TIMERA3_BASE, TIMER_A, mTagetPWMs.motor_1); // motor 1
        UpdateDutyCycle(TIMERA3_BASE, TIMER_B, mTagetPWMs.motor_2); // motor 2
        UpdateDutyCycle(TIMERA0_BASE, TIMER_A, mTagetPWMs.motor_3); // motor 3
        UpdateDutyCycle(TIMERA2_BASE, TIMER_B, mTagetPWMs.motor_4); // motor 4

        vTaskDelay(1);
    }
}
