#include <stdio.h>

// simplelink includes
#include "simplelink.h"

// driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "interrupt.h"
#include "hw_apps_rcm.h"
#include "prcm.h"
#include "uart.h"
#include "rom.h"
#include "rom_map.h"
#include "prcm.h"
#include "gpio.h"
#include "utils.h"
#include "timer.h"

// common interface includes
#include "common.h"
#include "gpio_if.h"
#include "uart_if.h"

// FreeRTOS includes
#include "osi.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// local includes
#include "pinmux.h"
#include "led.h"
#include "sensors.h"
#include "wifi.h"

/*/////////////////////////////////////////////////////////////////////////////
    Defines
/////////////////////////////////////////////////////////////////////////////*/

#define OSI_STACK_SIZE          524    // stack size in number of *words*
#define mainDELAY_LOOP_COUNT    100000
#define LOW_PRIORITY            0xFF

/*/////////////////////////////////////////////////////////////////////////////
    Variables
/////////////////////////////////////////////////////////////////////////////*/

extern void(* const g_pfnVectors[])(void);
extern unsigned char g_wifiConnected;
extern unsigned char g_wifiInitialized;

SemaphoreHandle_t  uart_lock;
SemaphoreHandle_t  wifi_lock;
volatile unsigned g_idle_cycle_count = 0UL;
volatile unsigned g_wifi_cycle_count = 0UL;
volatile unsigned g_motor_cycle_count = 0UL;
volatile unsigned g_gyro_cycle_count = 0UL;

/*/////////////////////////////////////////////////////////////////////////////
    Function Prototypes
/////////////////////////////////////////////////////////////////////////////*/

extern int gyro_init();
extern void WifiConnectTask(void *);
extern int motors_init();
extern void motors_ctrl(void *);

static void BoardInit(void);
void WifiIndicatorTask(void *pvParameters);

/*/////////////////////////////////////////////////////////////////////////////
    FreeRTOS Hook Definitions
/////////////////////////////////////////////////////////////////////////////*/

void vAssertCalled(const char *pcFile, unsigned long ulLine) {
    while(1) {}
}

void vApplicationIdleHook(void) {
    // can record task profiles here
    g_idle_cycle_count++;
}

void vApplicationMallocFailedHook(void) {
    UART_PRINT("[Error] FreeRTOS malloc failed\r\n");
    while(1) {}
}

void vApplicationStackOverflowHook(OsiTaskHandle *pxTask,
        signed char *pcTaskName) {
    UART_PRINT("[Error] FreeRTOS stack overflow\r\n");
    while(1) {}
}

/*/////////////////////////////////////////////////////////////////////////////
    Function Definitions
/////////////////////////////////////////////////////////////////////////////*/

static void BoardInit(void) {
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);

    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

void WifiIndicatorTask(void *pvParameters) {
    while(1)
    {
        if (g_wifiInitialized)
        {
            if (g_wifiConnected)
                setOrange();
            else
                toggleOrange();
        }
        vTaskDelay(100);
    }
}

/*/////////////////////////////////////////////////////////////////////////////
    Main Routine
/////////////////////////////////////////////////////////////////////////////*/

int
main()
{
    BoardInit();
    PinMuxConfig();
    InitTerm();
    UART_PRINT("Initializing CC3200 Quad Firmware\r\n");

    clearBlue();
    clearGreen();
    clearOrange();

    sensors_init();
    motors_init();

    uart_lock = xSemaphoreCreateMutex();
    wifi_lock = xSemaphoreCreateMutex();

    VStartSimpleLinkSpawnTask(SPAWN_TASK_PRIORITY);

    xTaskCreate( WifiIndicatorTask, NULL,
            OSI_STACK_SIZE, (void *) NULL, 5, NULL );
    xTaskCreate( WifiConnectTask, NULL,
            OSI_STACK_SIZE, NULL, 5, NULL );
    xTaskCreate( motors_ctrl, NULL,
            OSI_STACK_SIZE, (void *) NULL, 5, NULL );

    UART_PRINT("Starting FreeRTOS Scheduling\r\n");
    vTaskStartScheduler();

    while(1) {};    // should not exit
    return 0;       // stop compiler warning
}
